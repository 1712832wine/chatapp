<?php

namespace App\Http\Livewire;

use Livewire\Component;

class HomeComponent extends Component
{
    public $isClick = false;
    public function clicked() {
        if(!$isClick) $isClick = true;
    }
    public function render()
    {
        return view('livewire.home-component')->layout('layouts.chat');
    }
}
