<!DOCTYPE html>
<html lang="en">
<!-- Head -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1, shrink-to-fit=no, viewport-fit=cover">
    <title>Messenger - 2.0.0</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon/favicon.ico" type="image/x-icon">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700" rel="stylesheet">

    <!-- Template CSS -->
    <link rel="stylesheet" href="./assets/css/template.bundle.css">
    <link rel="stylesheet" href="./assets/css/template.dark.bundle.css" media="(prefers-color-scheme: dark)">
    @livewireStyles
</head>

<body>
    <!-- Layout -->
    <div class="layout overflow-hidden">
        <!-- Navigation -->
        @include('component.nav')
        <!-- Navigation -->

        <!-- Sidebar -->
        <aside class="sidebar bg-light">
            <div class="tab-content h-100" role="tablist">
                <!-- Create -->
                @include('component.sidebar-create')

                <!-- Friends -->
                @include('component.sidebar-friend')

                <!-- Chats -->
                @include('component.sidebar-chat')

                <!-- Notifications - Notices -->
                @include('component.sidebar-notification')

                <!-- Support -->
                @include('component.sidebar-support')

                <!-- Settings -->
                @include('component.sidebar-setting')
            </div>
        </aside>
        <!-- Sidebar -->
        {{ $slot }}

    </div>
    <!-- Layout -->

    <!-- Modal: Invite -->
    @include('component.modal-invite')

    <!-- Modal: Profile -->
    @include('component.modal-profile')

    <!-- Modal: User profile -->
    @include('component.modal-user-profile')

    <!-- Modal: Media Preview -->
    @include('component.modal-media-preview')

    <!-- Scripts -->
    <script src="./assets/js/vendor.js"></script>
    <script src="./assets/js/template.js"></script>
    @livewireScripts
</body>

</html>
